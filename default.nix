{ rust
, pkg-config
, openssl
, makeRustPlatform
}:
let
  rustPlatform = makeRustPlatform {
    rustc = rust;
    cargo = rust;
  };
in
rustPlatform.buildRustPackage {
  pname = "yt-channel-watcher";
  version = "0.1.0";
  src = ./.;

  nativeBuildInputs = [ pkg-config ];
  buildInputs = [ openssl.dev ];

  cargoBuildFlags = "-p yt-channel-watcher";
  cargoLock.lockFile = ./Cargo.lock;
}
