{ flakePackages }:
{ pkgs, lib, config, ... }:
with lib;
let
  cfg = config.services.yt-channel-watcher;
  system = pkgs.stdenv.hostPlatform.system;
in
{
  options.services.yt-channel-watcher = {
    enable = mkEnableOption "YouTube Channel Watcher";
    package = (mkPackageOption pkgs "yt-channel-watcher" { }) // {
      default = flakePackages.${system}.default;
    };

    channels = mkOption {
      description = "Channels to watch.";
      default = { };

      type = types.attrsOf (types.submodule (
        { name, config, ... }:
        {
          options = {
            channelId = mkOption {
              description = "YouTube channel ID.";
              type = types.str;
            };

            directory = mkOption {
              description = "Directory to store scrapes into.";
              type = types.path;
            };

            interval = mkOption {
              description = "Interval between scrapes in seconds.";
              type = types.int;
              default = 300;
            };
          };

          config = {
            channelId = mkDefault name;
            directory = mkDefault "/var/lib/yt-channel-watcher/${config.channelId}";
          };
        }
      ));
    };
  };

  config = mkIf cfg.enable {
    users.users.yt-channel-watcher = {
      description = "YouTube Channel Watcher service user";
      home = "/var/lib/yt-channel-watcher";
      createHome = true;
      isSystemUser = true;
      group = "yt-channel-watcher";
    };
    users.groups.yt-channel-watcher = { };

    systemd.services = mapAttrs'
      (name: cfg': {
        name = "yt-channel-watcher-${name}";
        value = {
          description = "YouTube Channel Watcher - ${name}";
          wantedBy = [ "multi-user.target" ];
          after = [ "network.target" ];

          serviceConfig = {
            ExecStart = "${cfg.package}/bin/yt-channel-watcher -i ${builtins.toString cfg'.interval} ${cfg'.channelId} ${cfg'.directory}";
            User = "yt-channel-watcher";
            Restart = "always";
          };
        };
      })
      cfg.channels;
  };
}
