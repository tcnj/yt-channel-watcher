use serde::Serialize;

use crate::fetch::Entry;

#[derive(Serialize)]
pub struct Upload {
    pub entry_id: String,
    pub video_id: String,
    pub title: String,
    pub url: String,
    pub published_at: String,
    pub updated_at: String,

    pub channel_id: String,
    pub channel_name: String,
    pub channel_url: String,

    pub content_title: String,
    pub content_url: String,
    pub content_type: String,
    pub content_width: u32,
    pub content_height: u32,

    pub thumnail_url: String,
    pub thumnail_width: u32,
    pub thumnail_height: u32,

    pub description: String,

    pub likes: u32,
    pub views: u32,
}

impl From<Entry> for Upload {
    fn from(value: Entry) -> Self {
        Upload {
            entry_id: value.id,
            video_id: value.video_id,
            title: value.title,
            url: value.link.href,
            published_at: value.published,
            updated_at: value.updated,
            channel_id: value.channel_id,
            channel_name: value.author.name,
            channel_url: value.author.uri,
            content_title: value.group.title,
            content_url: value.group.content.url,
            content_type: value.group.content.type_,
            content_width: value.group.content.width,
            content_height: value.group.content.height,
            thumnail_url: value.group.thumbnail.url,
            thumnail_width: value.group.thumbnail.width,
            thumnail_height: value.group.thumbnail.height,
            description: value.group.description,
            likes: value.group.community.star_rating.count,
            views: value.group.community.statistics.views,
        }
    }
}
