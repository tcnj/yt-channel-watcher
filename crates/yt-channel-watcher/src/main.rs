use std::{fs, path::PathBuf, thread::sleep, time::Duration};

use anyhow::Result;
use clap::Parser;
use schema::Upload;

mod fetch;
mod schema;

/// Watch a YouTube Channel for new uploads and write metadata to JSON files.
#[derive(Parser)]
struct Opts {
    /// YouTube Channel ID to watch the feed of
    channel_id: String,
    /// Directory in which to store metadata
    directory: PathBuf,

    /// Interval between scrapes in seconds
    #[clap(short, default_value = "300")]
    interval: u64,
}

const BASE_BACKOFF: Duration = Duration::from_secs(1);

fn main() -> Result<()> {
    let opts = Opts::parse();
    let wait_duration = Duration::from_secs(opts.interval);

    fs::create_dir_all(&opts.directory)?;

    let mut backoff_duration = BASE_BACKOFF;
    loop {
        let result = scrape(&opts);

        match result {
            Ok(()) => {
                backoff_duration = BASE_BACKOFF;
                println!(
                    "Successfully scraped channel, sleeping for {} seconds...",
                    opts.interval
                );
                sleep(wait_duration);
            }
            Err(e) => {
                eprintln!("Encountered error whilst scraping: {e:#?}");
                println!("Backing off for {} seconds...", backoff_duration.as_secs());
                sleep(backoff_duration);
                backoff_duration *= 2;
            }
        }
    }
}

fn scrape(opts: &Opts) -> Result<()> {
    let feed = fetch::fetch(&opts.channel_id)?;

    for entry in feed.entry {
        let upload = Upload::from(entry);
        let json_text = serde_json::to_string_pretty(&upload)?;

        let path = opts
            .directory
            .join(format!("{}-{}.json", upload.updated_at, upload.video_id));

        if !path.exists() {
            fs::write(path, json_text)?;
        }
    }

    Ok(())
}
