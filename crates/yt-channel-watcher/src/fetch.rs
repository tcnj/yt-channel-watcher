use anyhow::Result;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Feed {
    pub entry: Vec<Entry>,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Entry {
    pub id: String,
    pub video_id: String,
    pub channel_id: String,
    pub title: String,
    pub link: Link,
    pub author: Author,
    pub published: String,
    pub updated: String,
    pub group: Group,
}

#[derive(Deserialize, Debug)]
pub struct Link {
    #[serde(rename = "@href")]
    pub href: String,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Author {
    pub name: String,
    pub uri: String,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Group {
    pub title: String,
    pub content: Content,
    pub thumbnail: Thumbnail,
    pub description: String,
    pub community: Community,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Content {
    #[serde(rename = "@url")]
    pub url: String,
    #[serde(rename = "@type")]
    pub type_: String,
    #[serde(rename = "@width")]
    pub width: u32,
    #[serde(rename = "@height")]
    pub height: u32,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Thumbnail {
    #[serde(rename = "@url")]
    pub url: String,
    #[serde(rename = "@width")]
    pub width: u32,
    #[serde(rename = "@height")]
    pub height: u32,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Community {
    pub star_rating: StarRating,
    pub statistics: Statistics,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct StarRating {
    #[serde(rename = "@count")]
    pub count: u32,
    #[serde(rename = "@average")]
    pub average: f32,
    #[serde(rename = "@min")]
    pub min: u8,
    #[serde(rename = "@max")]
    pub max: u8,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Statistics {
    #[serde(rename = "@views")]
    pub views: u32,
}

pub fn fetch(channel_id: &str) -> Result<Feed> {
    let resp = reqwest::blocking::get(format!(
        "https://www.youtube.com/feeds/videos.xml?channel_id={channel_id}"
    ))?;

    let text = resp.text()?;

    let feed = quick_xml::de::from_str::<Feed>(&text)?;

    Ok(feed)
}
