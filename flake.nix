{
  inputs = {
    nixpkgs = {
      url = "github:NixOS/nixpkgs/nixos-24.11";
    };
    flake-parts = {
      url = "github:hercules-ci/flake-parts";
    };
    devshell = {
      url = "github:numtide/devshell";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ flake-parts, devshell, rust-overlay, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } ({ self, ... }: {
      imports = [
        devshell.flakeModule
      ];

      systems = [
        "x86_64-linux"
      ];

      flake = {
        nixosModules.default = import ./nixos.nix {
          flakePackages = self.packages;
        };
      };

      perSystem = { self', system, pkgs, ... }:
        let customRust = pkgs.rust-bin.stable.latest.default.override { extensions = [ "rust-src" ]; };
        in
        {
          _module.args.pkgs = import inputs.nixpkgs {
            inherit system;

            overlays = [
              inputs.rust-overlay.overlays.default
            ];

            config = { };
          };

          packages.default = pkgs.callPackage ./. { rust = customRust; };
          apps.default = {
            type = "app";
            program = "${self'.packages.default}/bin/yt-channel-watcher";
          };

          apps.devshell = self'.devShells.default.flakeApp;

          devshells.default = {
            packages = with pkgs; [
              customRust
              gcc
              pkg-config
              openssl
              openssl.dev
            ];

            env = [
              {
                name = "LD_LIBRARY_PATH";
                eval = "\${PATH:+\${PATH}:}$DEVSHELL_DIR/lib";
              }
              {
                name = "PKG_CONFIG_PATH";
                eval = "\${PKG_CONFIG_PATH:+\${PKG_CONFIG_PATH}:}$DEVSHELL_DIR/lib/pkgconfig";
              }
            ];
          };
        };
    })
  ;
}
